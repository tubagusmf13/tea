import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions ,RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/publishReplay';
import 'rxjs/add/operator/do';
import { Championship } from '../model/championship'

@Injectable()
export class ChamphionshipService {

  private chamionUrl = "http://localhost:8000/api/championship";
  //private chamionUrl = "http://192.168.100.33:8000/api/championship";

  constructor(private http: Http) {
  }

  getChampionship(): Observable<Championship[]> {
    return this.http.get(this.chamionUrl)
                    .map(res => {
                      return <Championship[]>res.json();})
                    .catch(this.handleError);
  }

  getChampionshipId(champId): Observable<Championship> {
    return this.http.get(this.chamionUrl+"/"+champId)
                    .map(res => {
                      return <Championship>res.json();})
                    .catch(this.handleError);
  }

  deleteChampionship(champId: Championship): Observable<number> {
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({headers: headers});
    return this.http.get(this.chamionUrl +"/del/"+ champId)
           .map(success => success.status)
                 .catch(this.handleError);        
      }	

  editChampionship(champId ,  body: Championship) {
    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' });
    let options = new RequestOptions({ headers: headers });
    let bodyStringJSON = JSON.stringify(body);
    console.log(bodyStringJSON);
    
    return this.http
      .post(this.chamionUrl + "/put/" + champId, bodyStringJSON, null)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json || 'Server Error' ));
  }

  private handleError(res) {
    return Observable.throw(res.statusText);
  } 


}
