import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions ,RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/publishReplay';
import 'rxjs/add/operator/do';
import { Contingent } from '../model/contingent'

@Injectable()
export class ContingentService {

  private contingentUrl = "http://localhost:8000/api/contingent";
 // private contingentUrl = "https://192.168.100.33:8000/api/contingent";
  
  

  constructor(private http: Http) {
  }

  getContingent(): Observable<Contingent[]> {
    return this.http.get(this.contingentUrl)
                    .map(res => { return <Contingent[]>res.json(); } )
                    .catch(this.handleError);
  }

  postContingent(body : Contingent){
    let bodyStringJSON = JSON.stringify(body);
      
    let headers = new Headers({'Content-Type': 'application/json'});
    let optionsa = new RequestOptions({headers: headers});
    const options = new RequestOptions({
      method: RequestMethod.Post,
      headers: headers
    });
    console.log(bodyStringJSON);
     
   return this.http.post(this.contingentUrl, bodyStringJSON,null)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json || 'Server Error' ));
  } 
  
  deleteContingent(contId: Contingent): Observable<number> {
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({headers: headers});
    return this.http.get(this.contingentUrl +"/del/"+ contId)
           .map(success => success.status)
                 .catch(this.handleError);        
      }	

  getContingentId(contId: Contingent): Observable<Contingent> {
    return this.http.get(this.contingentUrl+ "/" + contId).map(res => {
      return <Contingent>res.json();
    }).catch(this.handleError);
  }

  editContingent(contId ,  body: Contingent) {
    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' });
    let options = new RequestOptions({ headers: headers });
    let bodyStringJSON = JSON.stringify(body);
    console.log(bodyStringJSON);
    
    return this.http
      .post(this.contingentUrl + "/put/" + contId, bodyStringJSON, null)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json || 'Server Error' ));
  }

  
  private handleError(res) {
    return Observable.throw(res.statusText);
  } 

}
