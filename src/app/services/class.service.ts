import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions ,RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/publishReplay';
import 'rxjs/add/operator/do';
import { Classy } from '../model/classy'

@Injectable()
export class ClassService {

  private url = "http://localhost:8000/api/class";

  constructor(private http: Http) {  }

  getClass(grpId): Observable<Classy[]> {
      return this.http.get(this.url + "/" + "group" +"/"+grpId)
                  .map(res => {
                    return <Classy[]>res.json();})
                  .catch(this.handleError);
  }

  private handleError(res) {
    return Observable.throw(res.statusText);
  } 

}
