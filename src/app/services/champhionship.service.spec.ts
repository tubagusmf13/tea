import { TestBed, inject } from '@angular/core/testing';
import { ChamphionshipService } from './champhionship.service';

describe('ChamphionshipService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ChamphionshipService]
    });
  });

  it('should ...', inject([ChamphionshipService], (service: ChamphionshipService) => {
    expect(service).toBeTruthy();
  }));
});
