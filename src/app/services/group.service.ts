import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions ,RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/publishReplay';
import 'rxjs/add/operator/do';
import { Group } from '../model/group'

@Injectable()
export class GroupService {

  private url = "http://localhost:8000/api/group";
  //private url = "http://192.168.100.33:8000/api/group";


  constructor(private http: Http) {  }

  getGroup(): Observable<Group[]> {
      return this.http.get(this.url)
                  .map(res => {
                    return <Group[]>res.json();})
                  .catch(this.handleError);
  }
  
  private handleError(res) {
    return Observable.throw(res.statusText);
  } 

}
