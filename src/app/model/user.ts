export class User {
    status: string;
    loginAs: string;
    officialId: number;
    username: string;
    password: string;
}