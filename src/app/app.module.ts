import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule,  ReactiveFormsModule } from '@angular/forms';
import { PopupModule } from 'ng2-opd-popup';

import { HttpModule } from '@angular/http';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { DataPageComponent } from './component/page-tekmit/data-page/data-page.component';
import { SidebarComponent } from './component/sidebar/sidebar.component';
import { NavbarComponent } from './component/navbar/navbar.component';
import { FooterComponent } from './component/footer/footer.component';
import { CompetitionSchemePageComponent } from './component/page-tekmit/competition-scheme-page/competition-scheme-page.component';
import { DetailCompetitionPageComponent } from './component/page-tekmit/detail-competition-page/detail-competition-page.component';
import { ReportTekmitComponent } from './component/page-tekmit/report-tekmit/report-tekmit.component';
import { MonitoringCompetitionComponent } from './component/page-match/monitoring-competition/monitoring-competition.component';
import { ReportMatchComponent } from './component/page-match/report-match/report-match.component';
import { ViewPenontonComponent } from './component/page-match/view-penonton/view-penonton.component';
import { ViewDewanJuriComponent } from './component/page-match/view-dewan-juri/view-dewan-juri.component';
import { HomePageComponent } from './component/page-online/home-page/home-page.component';
import { NavbarOnlineComponent } from './component/page-online/navbar-online/navbar-online.component';
import { FooterOnlineComponent } from './component/page-online/footer-online/footer-online.component';
import { EventChampionshipComponent } from './component/page-online/event-championship/event-championship.component';
import { RegisterSelectChampionshipComponent } from './component/page-online/register-select-championship/register-select-championship.component';
import { RegisterContingentComponent } from './component/page-online/register-contingent/register-contingent.component';
import { RegisterParticipantComponent } from './component/page-online/register-participant/register-participant.component';
import { PageLoginComponent } from './component/page-online/page-login/page-login.component';
import { DataChampionshipComponent } from './component/page-tekmit/data-page/data-championship/data-championship.component';
import { DataContingentComponent } from './component/page-tekmit/data-page/data-contingent/data-contingent.component';
import { DataJurorComponent } from './component/page-tekmit/data-page/data-juror/data-juror.component';
import { DataParticipantComponent } from './component/page-tekmit/data-page/data-participant/data-participant.component';


import { MessageService } from './services/message.service';
import { ParticipantService } from './services/participant.service';
import { ContingentService } from './services/contingent.service';
import { ChamphionshipService } from './services/champhionship.service';
import { GroupService } from './services/group.service';
import { ClassService } from './services/class.service';
import { LoginService } from './services/login.service';
import { AuthGuard } from './services/auth-guard.service';
import { JurorService } from './services/juror.service';

import { ReportClassChampionComponent } from './component/page-match/report-match/report-class-champion/report-class-champion.component';
import { ReportComptScheduleComponent } from './component/page-match/report-match/report-compt-schedule/report-compt-schedule.component';
import { ReportComptSchemeComponent } from './component/page-match/report-match/report-compt-scheme/report-compt-scheme.component';
import { ReportMedalListComponent } from './component/page-match/report-match/report-medal-list/report-medal-list.component';
import { ReportWeightValidationComponent } from './component/page-match/report-match/report-weight-validation/report-weight-validation.component';
import { ReportCompetitionSchemeComponent } from './component/page-tekmit/report-tekmit/report-competition-scheme/report-competition-scheme.component';
import { ReportListParticipantComponent } from './component/page-tekmit/report-tekmit/report-list-participant/report-list-participant.component';
import { ViewStatisticsComponent } from './component/page-match/view-statistics/view-statistics.component';
import { SignInComponent } from './component/sign-in/sign-in.component';


@NgModule({
  declarations: [
    AppComponent,
    DataPageComponent,
    SidebarComponent,
    NavbarComponent,
    FooterComponent,
    CompetitionSchemePageComponent,
    DetailCompetitionPageComponent,
    ReportTekmitComponent,
    MonitoringCompetitionComponent,
    ReportMatchComponent,
    ViewPenontonComponent,
    ViewDewanJuriComponent,
    HomePageComponent,
    NavbarOnlineComponent,
    FooterOnlineComponent,
    EventChampionshipComponent,
    RegisterSelectChampionshipComponent,
    RegisterContingentComponent,
    RegisterParticipantComponent,
    PageLoginComponent,
    DataChampionshipComponent,
    DataContingentComponent,
    DataJurorComponent,
    DataParticipantComponent,
    ReportClassChampionComponent,
    ReportComptScheduleComponent,
    ReportComptSchemeComponent,
    ReportMedalListComponent,
    ReportWeightValidationComponent,
    ReportCompetitionSchemeComponent,
    ReportListParticipantComponent,
    ViewStatisticsComponent,
    SignInComponent,

    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpModule,
    ReactiveFormsModule,
    PopupModule.forRoot()
  ],
  providers: [MessageService , AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
