import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { DataPageComponent } from './component/page-tekmit/data-page/data-page.component';
import { DetailCompetitionPageComponent } from './component/page-tekmit/detail-competition-page/detail-competition-page.component';
import { ReportTekmitComponent } from './component/page-tekmit/report-tekmit/report-tekmit.component';
import { CompetitionSchemePageComponent } from './component/page-tekmit/competition-scheme-page/competition-scheme-page.component';
import { MonitoringCompetitionComponent } from './component/page-match/monitoring-competition/monitoring-competition.component';
import { ReportMatchComponent } from './component/page-match/report-match/report-match.component';
import { ViewPenontonComponent } from './component/page-match/view-penonton/view-penonton.component';
import { ViewDewanJuriComponent } from './component/page-match/view-dewan-juri/view-dewan-juri.component';
import { HomePageComponent } from './component/page-online/home-page/home-page.component';
import { NavbarOnlineComponent } from './component/page-online/navbar-online/navbar-online.component';
import { FooterOnlineComponent } from './component/page-online/footer-online/footer-online.component';
import { EventChampionshipComponent } from './component/page-online/event-championship/event-championship.component';
import { RegisterSelectChampionshipComponent } from './component/page-online/register-select-championship/register-select-championship.component';
import { RegisterContingentComponent } from './component/page-online/register-contingent/register-contingent.component';
import { RegisterParticipantComponent } from './component/page-online/register-participant/register-participant.component';
import { PageLoginComponent } from './component/page-online/page-login/page-login.component';
import { DataChampionshipComponent } from './component/page-tekmit/data-page/data-championship/data-championship.component';
import { DataContingentComponent } from './component/page-tekmit/data-page/data-contingent/data-contingent.component';
import { DataJurorComponent } from './component/page-tekmit/data-page/data-juror/data-juror.component';
import { DataParticipantComponent } from './component/page-tekmit/data-page/data-participant/data-participant.component';
import { ReportClassChampionComponent } from './component/page-match/report-match/report-class-champion/report-class-champion.component';
import { ReportComptScheduleComponent } from './component/page-match/report-match/report-compt-schedule/report-compt-schedule.component';
import { ReportComptSchemeComponent } from './component/page-match/report-match/report-compt-scheme/report-compt-scheme.component';
import { ReportMedalListComponent } from './component/page-match/report-match/report-medal-list/report-medal-list.component';
import { ReportWeightValidationComponent } from './component/page-match/report-match/report-weight-validation/report-weight-validation.component';
import { ReportCompetitionSchemeComponent } from './component/page-tekmit/report-tekmit/report-competition-scheme/report-competition-scheme.component';
import { ReportListParticipantComponent } from './component/page-tekmit/report-tekmit/report-list-participant/report-list-participant.component';
import { ViewStatisticsComponent } from './component/page-match/view-statistics/view-statistics.component';

import { AuthGuard } from './services/auth-guard.service';
import { SignInComponent } from './component/sign-in/sign-in.component';


const routes: Routes =
  [
    { path : '', redirectTo: '/signin', pathMatch: 'full'},
    { path: 'signin', component: SignInComponent},
    { path: 'data', component: DataPageComponent},
    { path: 'competition-scheme', component: CompetitionSchemePageComponent},
    { path: 'detail-competition', component: DetailCompetitionPageComponent},
    { path: 'report', component: ReportTekmitComponent},
    { path: 'monitoring-competition', component: MonitoringCompetitionComponent},
    { path: 'report-match', component: ReportMatchComponent},
    { path: 'view-penonton', component: ViewPenontonComponent},
    { path: 'view-dewan-juri', component: ViewDewanJuriComponent},
    { path: 'home',  component: HomePageComponent },
    { path: 'championship',  component: RegisterSelectChampionshipComponent, canActivate: [AuthGuard]},
    { path: 'contingent',  component: RegisterContingentComponent, canActivate: [AuthGuard]},
    { path: 'event',  component: EventChampionshipComponent },
    { path: 'participant',  component: RegisterParticipantComponent, canActivate: [AuthGuard]},
    { path: 'login',  component: PageLoginComponent },
    { path: 'data-championship',  component: DataChampionshipComponent }, 
    { path: 'data-contingent',  component: DataContingentComponent },
    { path: 'data-participant',  component: DataParticipantComponent },
    { path: 'data-juror',  component: DataJurorComponent },
    { path: 'report-list-participant', component: ReportListParticipantComponent},
    { path: 'report-competition-scheme', component: ReportCompetitionSchemeComponent},
    { path: 'report-compt-scheme', component: ReportComptSchemeComponent},
    { path: 'report-compt-schedule', component: ReportComptScheduleComponent},
    { path: 'report-class-champion', component: ReportClassChampionComponent},
    { path: 'report-weight-validation', component: ReportWeightValidationComponent},
    { path: 'report-medal-list', component: ReportMedalListComponent},
    { path: 'view-statistics', component: ViewStatisticsComponent},
    
  ];

@NgModule({
  imports: [ RouterModule.forRoot(routes,{ useHash: true })],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
