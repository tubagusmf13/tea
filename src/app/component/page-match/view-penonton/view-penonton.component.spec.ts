import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewPenontonComponent } from './view-penonton.component';

describe('ViewPenontonComponent', () => {
  let component: ViewPenontonComponent;
  let fixture: ComponentFixture<ViewPenontonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewPenontonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewPenontonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
