import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl, FormBuilder, Validators, Form, FormArray} from '@angular/forms';
import { GroupService } from '../../../services/group.service';
import { ClassService } from '../../../services/class.service';
import { ParticipantService } from '../../../services/participant.service';
import { ContingentService } from '../../../services/contingent.service';

import { Participant } from '../../../model/participant';
import { Group } from '../../../model/group';
import { Classy } from '../../../model/classy';
import { Contingent } from '../../../model/contingent';

@Component({
  selector: 'app-register-participant',
  templateUrl: './register-participant.component.html',
  styleUrls: ['./register-participant.component.css'],
  providers: [ParticipantService,GroupService,ClassService,ContingentService]

})
export class RegisterParticipantComponent implements OnInit {

  private participants: Participant[];
  private groups: Group[];
  private classs: Classy[];
  public formAdd: FormGroup;
  public items: FormArray;
  public isSumbitted : boolean;

  constructor(private _formBuilder : FormBuilder, 
              private partService : ParticipantService,
              private grupService: GroupService,
              private contService: ContingentService,
              private clsService: ClassService) { }

  ngOnInit() {
    this.getGroups();
    //this.getClasss();

    console.log("PARTICIPANTS POST SERVER");

    this.formAdd = this._formBuilder.group({
      groupIdP: [''],
      items: this._formBuilder.array([])
        });

    this.items= <FormArray>this.formAdd.controls['items'];
    this.isSumbitted=false;

  }

  createItem(classId :any,className :any,groupId :any,gender :any): FormGroup {
    return this._formBuilder.group({        
        classId: [classId],
        className: [className],
        participantName: ['', <any>Validators.required],
        groupId: [groupId],
        age: ['',[<any>Validators.required,<any>Validators.maxLength(2)]],
        gender: [gender],
        placeOfBirth: ['', <any>Validators.required],
        dateOfBirth: ['', <any>Validators.required],
        height: ['',[<any>Validators.required,<any>Validators.maxLength(3)]],
        weight: ['',[<any>Validators.required,<any>Validators.minLength(2)]],
        address: ['', <any>Validators.required],
      }
      
    );

  }
  
  onSubmit(participant: any): void {
   // let partic : Participant= 

    console.log(JSON.stringify(participant));
    let participants : Participant[]= [];
    for(let i=0; i < participant.items.length; i++ ){
      let part = new Participant();
      part.address = participant.items[i].address;
      part.age = participant.items[i].age;
      part.classId = participant.items[i].classId;
      part.dateOfBirth = participant.items[i].dateOfBirth;
      part.placeOfBirth = participant.items[i].placeOfBirth;
      part.height = participant.items[i].height;
      part.weight = participant.items[i].weight;
      part.gender = participant.items[i].gender;
      part.participantName = participant.items[i].participantName;
      part.contingentId = <number>JSON.parse(localStorage.getItem("regCont"));
      participants.push(part);
      this.isSumbitted=true;

    }

    this.partService
    .postParticipant(participants).subscribe(
      res=>{
        console.log(res); 
      if(res.status == "success"){alert("Register Succes !")}
      else{alert("Register Failed !")} },
       err=>console.log(err));
      alert(JSON.stringify(participant));
  }

  addMore(item:any): void {
    //this.items.push(this.createItem());
  }

  selGroup(event : any):void {
    let grpId = event.target.value;
    alert("View"+grpId);
    this.getClasss(grpId);
  }

  getGroups(): void {
    console.log("GROUP FROM SERVER");
    this.grupService.getGroup()
    .subscribe(cat =>{this.groups=cat;
  } , er => console.log(er));	
  }

  getClasss(grpId:Number): void {
    console.log("CLASS FROM SERVER");
    this.clsService.getClass(grpId)
    .subscribe(cat =>{this.classs=cat; 
      this.classs.forEach((value) => {
        this.items.push(this.createItem(value.classId,value.className,value.groupId,value.gender)); });
    } , er => console.log(er));	
  }

}
