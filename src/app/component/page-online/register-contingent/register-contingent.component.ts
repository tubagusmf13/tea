import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { FormGroup, FormControl, FormBuilder, Validators} from '@angular/forms';
import { Contingent } from '../../../model/contingent';
import { Observable} from 'rxjs/Observable';
import { ContingentService } from '../../../services/contingent.service';
import { RouterLink } from '@angular/router';

@Component({
  selector: 'app-register-contingent',
  templateUrl: './register-contingent.component.html',
  styleUrls: ['./register-contingent.component.css'],
  providers: [ContingentService]

})
export class RegisterContingentComponent implements OnInit {

  public formAdd: FormGroup;  
  private keyCont="contingents";
  private contingents: Contingent[];
  public isSumbitted : boolean;

  constructor(private routes : Router, private _formBuilder : FormBuilder, private contService: ContingentService) { }

  ngOnInit() {
    this.formAdd = this._formBuilder.group({
      contingentName: ['', <any>Validators.required],
      contingentAddress: ['', <any>Validators.required],
      participantAmount: ['', <any>Validators.required],
      officialName: ['', <any>Validators.required],
      officialId: [ <string>JSON.parse(localStorage.getItem("idChamp"))],
    });
    console.log(this.formAdd);
    this.isSumbitted=false;

  }

  onSubmit(contingent: Contingent): void {
    this.isSumbitted=true;

    this.contService.postContingent(contingent).subscribe(
      res=>{ alert(JSON.stringify(res)); 
      if(res.status == "success"){
        alert("Register Succes !"); 
        localStorage.setItem("regCont",JSON.stringify(res.contingentId));
        this.routes.navigate(['/participant']);

      }
      else{alert("Register Failed !")} },
        err=>console.log(err));
      alert(JSON.stringify(localStorage.getItem("regCont")));
  }

}
