import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavbarOnlineComponent } from './navbar-online.component';

describe('NavbarOnlineComponent', () => {
  let component: NavbarOnlineComponent;
  let fixture: ComponentFixture<NavbarOnlineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavbarOnlineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavbarOnlineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
