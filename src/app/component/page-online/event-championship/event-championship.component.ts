import { Component, OnInit } from '@angular/core';
import { ChamphionshipService } from '../../../services/champhionship.service';

import { Championship } from '../../../model/championship';
import { Observable} from 'rxjs/Observable';

@Component({
  selector: 'app-event-championship',
  templateUrl: './event-championship.component.html',
  styleUrls: ['./event-championship.component.css'],
  providers: [ChamphionshipService]

})
export class EventChampionshipComponent implements OnInit {

  private championships: Championship[];
  private keyChamp="champhionship";

  constructor(private champService : ChamphionshipService) { }

  ngOnInit() {

    this.getChampions();

  }

  selGroup(event : any):void {
    let champhionshipId = event.target.value;
    alert("View"+champhionshipId);
    localStorage.setItem("idChamp",JSON.stringify(champhionshipId)); 
  }

  getChampions(): void {
    console.log("CHAMPIONSHIP FROM SERVER");
    this.champService.getChampionship()
                     .subscribe(champ =>{this.championships=champ; localStorage
                     .setItem(this.keyChamp, JSON
                     .stringify(champ));}, er => console.log(er));	
  }

}
