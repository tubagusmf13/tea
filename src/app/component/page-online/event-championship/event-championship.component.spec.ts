import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventChampionshipComponent } from './event-championship.component';

describe('EventChampionshipComponent', () => {
  let component: EventChampionshipComponent;
  let fixture: ComponentFixture<EventChampionshipComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventChampionshipComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventChampionshipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
