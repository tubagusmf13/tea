import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterOnlineComponent } from './footer-online.component';

describe('FooterOnlineComponent', () => {
  let component: FooterOnlineComponent;
  let fixture: ComponentFixture<FooterOnlineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FooterOnlineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterOnlineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
