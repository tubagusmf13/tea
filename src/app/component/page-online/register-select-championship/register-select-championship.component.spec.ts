import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterSelectChampionshipComponent } from './register-select-championship.component';

describe('RegisterSelectChampionshipComponent', () => {
  let component: RegisterSelectChampionshipComponent;
  let fixture: ComponentFixture<RegisterSelectChampionshipComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterSelectChampionshipComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterSelectChampionshipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
