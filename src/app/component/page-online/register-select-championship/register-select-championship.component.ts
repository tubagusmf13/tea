import { Component, OnInit } from '@angular/core';
import { ChamphionshipService } from '../../../services/champhionship.service';

import { Championship } from '../../../model/championship';
import { Observable} from 'rxjs/Observable';



@Component({
  selector: 'app-register-select-championship',
  templateUrl: './register-select-championship.component.html',
  styleUrls: ['./register-select-championship.component.css'],
  providers: [ChamphionshipService]

})
export class RegisterSelectChampionshipComponent implements OnInit {

  private championships: Championship[];
  private keyChamp="champhionship";

  constructor(private champService : ChamphionshipService) { }

  ngOnInit() {

    this.getChampions();

  }

  selGroup(event : any):void {
    let champhionshipId = event.target.value;
    alert("View"+champhionshipId);
    localStorage.setItem("idChamp",JSON.stringify(champhionshipId)); 
  }

  getChampions(): void {
    console.log("CHAMPIONSHIP FROM SERVER");
    this.champService.getChampionship()
                     .subscribe(champ =>{this.championships=champ; localStorage
                     .setItem(this.keyChamp, JSON
                     .stringify(champ));}, er => console.log(er));	
  }

}
