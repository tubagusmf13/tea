import { Component, OnInit } from '@angular/core';
import { ChamphionshipService } from '../../../services/champhionship.service';

import { Championship } from '../../../model/championship';
import { Observable} from 'rxjs/Observable';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css'],
  providers: [ChamphionshipService]
})
export class HomePageComponent implements OnInit {

  private championships: Championship[];

  constructor(private champService : ChamphionshipService) { }

  ngOnInit() {

    this.getChampions();

  }

  getChampions(): void {
    console.log("CHAMPIONSHIP FROM SERVER");
    this.champService.getChampionship()
    .subscribe(jur =>{this.championships=jur; }, er => console.log(er));
  }

}
