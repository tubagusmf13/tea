import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl, FormBuilder, Validators} from '@angular/forms';
import { ParticipantService } from '../../../services/participant.service';
import { MessageService } from '../../../services/message.service';
import { ContingentService } from '../../../services/contingent.service';

import { Participant } from '../../../model/participant';
import { Contingent } from '../../../model/contingent';
import { Observable} from 'rxjs/Observable';

@Component({
  selector: 'app-data-page',
  templateUrl: './data-page.component.html',
  styleUrls: ['./data-page.component.css'],
  providers: [ParticipantService,ContingentService]
})
export class DataPageComponent implements OnInit {

  private participants: Participant[];
  private contingents: Contingent[];
  private errorMessage: string;
  public formAdd: FormGroup;
  statusCode: number;
  
  constructor(private _formBuilder : FormBuilder, private partService : ParticipantService , private contService: ContingentService , private msgService: MessageService) { }

  ngOnInit() {
  }

  getParticipants(): void {
    console.log("PARTICIPANTS FROM SERVER");
  	this.partService.getParticipant().subscribe(cat =>{this.participants=cat;} , er => console.log(er));	
  }

  getContingents(): void {
    console.log("CONTINGENTS FROM SERVER");
  	this.contService.getContingent().subscribe(cat =>{this.contingents=cat;} , er => console.log(er));
  }

  postContingents(): void {
    console.log("PARTICIPANTS POST SERVER");

    this.formAdd = this._formBuilder.group({
      contingentId: [''],
      contingentName: [''],
      participantAmount: ['']
    });
  }

  deleteContingents(contsId: Contingent) {
    this.contService.deleteContingent(contsId)
      .subscribe(cat => {this.statusCode = 204;
    },
    er => console.log(er));
  }

  onSubmit(contingent: Contingent): void {
    this.contService.postContingent(contingent).subscribe(
      res=>{
        console.log(res); 
      if(res.status == "success"){alert("Register Succes !")}
      else{alert("Register Failed !")} },
       err=>console.log(err));
      console.log(JSON.stringify(contingent));
  }


}
