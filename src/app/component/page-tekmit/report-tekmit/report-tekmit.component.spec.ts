import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportTekmitComponent } from './report-tekmit.component';

describe('ReportTekmitComponent', () => {
  let component: ReportTekmitComponent;
  let fixture: ComponentFixture<ReportTekmitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportTekmitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportTekmitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
