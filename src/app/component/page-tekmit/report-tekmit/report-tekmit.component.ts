import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ParticipantService } from '../../../services/participant.service';

import { Participant } from '../../../model/participant';

@Component({
  selector: 'app-report-tekmit',
  templateUrl: './report-tekmit.component.html',
  styleUrls: ['./report-tekmit.component.css'],
  providers: [ParticipantService]
})
export class ReportTekmitComponent implements OnInit {

  private participants: Participant[];

  constructor(private routes : Router, private partService : ParticipantService,) { }

  ngOnInit() {
    
  }

  getSelected(kelas,gender){
    console.log("participant FROM SERVER");
    this.partService.getParticipantClassGender(kelas,gender)
    .subscribe(res =>{ localStorage.setItem("reportPart",JSON.stringify(res));} , er => console.log(er));	
  
    alert(JSON.stringify(localStorage.getItem("reportPart")));
    this.routes.navigate(['/report-list-participant']);
  
  }

}
