import { Component, Renderer, OnInit, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';

@Component({
  selector: 'app-competition-scheme-page',
  templateUrl: './competition-scheme-page.component.html',
  styleUrls: ['./competition-scheme-page.component.css']
})
export class CompetitionSchemePageComponent implements OnInit {

   constructor(private _renderer2: Renderer, @Inject(DOCUMENT) private _document : any) {

    }

   ngOnInit() {
        let s = this._renderer2.createElement(this._renderer2, 'script');
        s.type = `text/javascript`;
        s.src = `assets/js/app-js/bagan.js`;
        
        this._renderer2.append(this._document.body, s);
   }

}