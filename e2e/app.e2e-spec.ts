import { TAAngularPage } from './app.po';

describe('ta-angular App', () => {
  let page: TAAngularPage;

  beforeEach(() => {
    page = new TAAngularPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
